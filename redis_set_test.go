package exredis

import (
	"reflect"
	"testing"

	"github.com/gomodule/redigo/redis"
)

func TestRedis_SADD(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key     string
		member  string
		members []string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.SADD(tt.args.key, tt.args.member, tt.args.members...)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.SADD() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.SADD() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_SCARD(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.SCARD(tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.SCARD() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.SCARD() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_SDIFF(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key  string
		keys []string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.SDIFF(tt.args.key, tt.args.keys...)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.SDIFF() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Redis.SDIFF() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_SDIFFSTORE(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		destination string
		key         string
		keys        []string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.SDIFFSTORE(tt.args.destination, tt.args.key, tt.args.keys...)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.SDIFFSTORE() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.SDIFFSTORE() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_SINTER(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key  string
		keys []string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.SINTER(tt.args.key, tt.args.keys...)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.SINTER() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Redis.SINTER() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_SINTERSTORE(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		destination string
		key         string
		keys        []string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.SINTERSTORE(tt.args.destination, tt.args.key, tt.args.keys...)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.SINTERSTORE() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.SINTERSTORE() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_SISMEMBER(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key    string
		member string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.SISMEMBER(tt.args.key, tt.args.member)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.SISMEMBER() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.SISMEMBER() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_SMEMBERS(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.SMEMBERS(tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.SMEMBERS() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Redis.SMEMBERS() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_SMOVE(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		source      string
		destination string
		member      string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.SMOVE(tt.args.source, tt.args.destination, tt.args.member)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.SMOVE() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.SMOVE() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_SPOP(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.SPOP(tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.SPOP() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.SPOP() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_SRANDMEMBER(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key   string
		count []int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.SRANDMEMBER(tt.args.key, tt.args.count...)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.SRANDMEMBER() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Redis.SRANDMEMBER() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_SREM(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key     string
		member  string
		members []string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.SREM(tt.args.key, tt.args.member, tt.args.members...)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.SREM() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.SREM() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_SUNION(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key  string
		keys []string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.SUNION(tt.args.key, tt.args.keys...)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.SUNION() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Redis.SUNION() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_SUNIONSTORE(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		destination string
		key         string
		keys        []string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.SUNIONSTORE(tt.args.destination, tt.args.key, tt.args.keys...)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.SUNIONSTORE() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.SUNIONSTORE() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_SSCAN(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	tests := []struct {
		name   string
		fields fields
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			r.SSCAN()
		})
	}
}
