package exredis

import (
	"reflect"
	"testing"

	"github.com/gomodule/redigo/redis"
)

func TestRedis_DEL(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		keys []interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.DEL(tt.args.keys...)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.DEL() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.DEL() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_DUMP(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.DUMP(tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.DUMP() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.DUMP() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_EXISTS(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.EXISTS(tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.EXISTS() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.EXISTS() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_EXPIRE(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key     string
		seconds int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.EXPIRE(tt.args.key, tt.args.seconds)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.EXPIRE() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.EXPIRE() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_EXPIREAT(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key           string
		unixTimestamp int64
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.EXPIREAT(tt.args.key, tt.args.unixTimestamp)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.EXPIREAT() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.EXPIREAT() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_KEYS(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		pattern string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []string
		wantErr bool
	}{
		// TODO: Add test cases.
		{"case 0", fields{MyRedis.pool}, args{"*"}, []string{"123"}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.KEYS(tt.args.pattern)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.KEYS() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Redis.KEYS() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_MIGRATE(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		host      string
		port      string
		key       string
		db        string
		timeoutMS int64
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.MIGRATE(tt.args.host, tt.args.port, tt.args.key, tt.args.db, tt.args.timeoutMS)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.MIGRATE() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.MIGRATE() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_MOVE(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key string
		db  string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.MOVE(tt.args.key, tt.args.db)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.MOVE() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.MOVE() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_OBJECTREFCOUNT(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.OBJECTREFCOUNT(tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.OBJECTREFCOUNT() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.OBJECTREFCOUNT() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_OBJECTENCODING(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.OBJECTENCODING(tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.OBJECTENCODING() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.OBJECTENCODING() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_OBJECTIDLETIME(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int64
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.OBJECTIDLETIME(tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.OBJECTIDLETIME() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.OBJECTIDLETIME() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_PERSIST(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.PERSIST(tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.PERSIST() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.PERSIST() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_PEXPIRE(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key          string
		milliseconds int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.PEXPIRE(tt.args.key, tt.args.milliseconds)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.PEXPIRE() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.PEXPIRE() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_PEXPIREAT(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key             string
		unixTimestampMS int64
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.PEXPIREAT(tt.args.key, tt.args.unixTimestampMS)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.PEXPIREAT() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.PEXPIREAT() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_PTTL(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int64
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.PTTL(tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.PTTL() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.PTTL() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_RANDOMKEY(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	tests := []struct {
		name    string
		fields  fields
		want    []string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.RANDOMKEY()
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.RANDOMKEY() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Redis.RANDOMKEY() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_RENAME(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key    string
		newkey string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.RENAME(tt.args.key, tt.args.newkey)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.RENAME() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.RENAME() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_RENAMENX(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key    string
		newkey string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.RENAMENX(tt.args.key, tt.args.newkey)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.RENAMENX() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.RENAMENX() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_RESTORE(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key     string
		ttl     string
		value   string
		replace bool
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.RESTORE(tt.args.key, tt.args.ttl, tt.args.value, tt.args.replace)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.RESTORE() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.RESTORE() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_SORT(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		args []interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.SORT(tt.args.args...)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.SORT() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Redis.SORT() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_TTL(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int64
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.TTL(tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.TTL() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.TTL() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_TYPE(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.TYPE(tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.TYPE() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.TYPE() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_SCAN(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	tests := []struct {
		name   string
		fields fields
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			r.SCAN()
		})
	}
}
