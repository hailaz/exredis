package exredis

import (
	"reflect"
	"testing"

	"github.com/gomodule/redigo/redis"
)

func TestRedis_HDEL(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key    string
		field  string
		fields []string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.HDEL(tt.args.key, tt.args.field, tt.args.fields...)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.HDEL() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.HDEL() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_HEXISTS(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.HEXISTS(tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.HEXISTS() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.HEXISTS() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_HGET(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.HGET(tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.HGET() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.HGET() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_HGETALL(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    map[string]string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.HGETALL(tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.HGETALL() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Redis.HGETALL() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_HINCRBY(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key       string
		field     string
		decrement int64
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.HINCRBY(tt.args.key, tt.args.field, tt.args.decrement)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.HINCRBY() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.HINCRBY() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_HINCRBYFLOAT(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key       string
		decrement float32
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.HINCRBYFLOAT(tt.args.key, tt.args.decrement)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.HINCRBYFLOAT() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.HINCRBYFLOAT() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_HKEYS(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.HKEYS(tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.HKEYS() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Redis.HKEYS() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_HLEN(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int64
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.HLEN(tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.HLEN() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.HLEN() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_HMGET(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key    string
		field  string
		fields []string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.HMGET(tt.args.key, tt.args.field, tt.args.fields...)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.HMGET() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Redis.HMGET() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_HMSET(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key    string
		field  string
		value  string
		fields []string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.HMSET(tt.args.key, tt.args.field, tt.args.value, tt.args.fields...)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.HMSET() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.HMSET() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_HSET(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key   string
		field string
		value string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.HSET(tt.args.key, tt.args.field, tt.args.value)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.HSET() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.HSET() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_HSETNX(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key   string
		field string
		value string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.HSETNX(tt.args.key, tt.args.field, tt.args.value)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.HSETNX() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.HSETNX() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_HVALS(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.HVALS(tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.HVALS() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Redis.HVALS() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_HSCAN(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	tests := []struct {
		name   string
		fields fields
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			r.HSCAN()
		})
	}
}

func TestRedis_HSTRLEN(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key   string
		field string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int64
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.HSTRLEN(tt.args.key, tt.args.field)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.HSTRLEN() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.HSTRLEN() = %v, want %v", got, tt.want)
			}
		})
	}
}
