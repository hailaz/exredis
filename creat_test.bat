gotests -all -w redis_key.go redis_key_test.go
gotests -all -w redis_string.go redis_string_test.go
gotests -all -w redis_hash.go redis_hash_test.go
gotests -all -w redis_list.go redis_list_test.go
gotests -all -w redis_set.go redis_set_test.go
gotests -all -w redis_sorted_set.go redis_sorted_set_test.go