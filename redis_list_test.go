package exredis

import (
	"reflect"
	"testing"

	"github.com/gomodule/redigo/redis"
)

func TestRedis_BLPOP(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key     string
		timeout string
		keys    []string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.BLPOP(tt.args.key, tt.args.timeout, tt.args.keys...)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.BLPOP() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Redis.BLPOP() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_BRPOP(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key     string
		timeout string
		keys    []string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.BRPOP(tt.args.key, tt.args.timeout, tt.args.keys...)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.BRPOP() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Redis.BRPOP() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_BRPOPLPUSH(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		source      string
		destination string
		timeout     string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.BRPOPLPUSH(tt.args.source, tt.args.destination, tt.args.timeout)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.BRPOPLPUSH() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Redis.BRPOPLPUSH() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_LINDEX(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key   string
		index string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.LINDEX(tt.args.key, tt.args.index)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.LINDEX() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.LINDEX() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_LINSERT(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key           string
		BEFOREorAFTER string
		pivot         string
		value         string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.LINSERT(tt.args.key, tt.args.BEFOREorAFTER, tt.args.pivot, tt.args.value)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.LINSERT() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.LINSERT() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_LLEN(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.LLEN(tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.LLEN() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.LLEN() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_LPOP(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.LPOP(tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.LPOP() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.LPOP() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_LPUSH(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key    string
		field  string
		fields []string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.LPUSH(tt.args.key, tt.args.field, tt.args.fields...)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.LPUSH() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.LPUSH() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_LPUSHX(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key   string
		field string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.LPUSHX(tt.args.key, tt.args.field)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.LPUSHX() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.LPUSHX() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_LRANGE(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key   string
		start string
		stop  string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.LRANGE(tt.args.key, tt.args.start, tt.args.stop)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.LRANGE() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Redis.LRANGE() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_LREM(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key   string
		count string
		value string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.LREM(tt.args.key, tt.args.count, tt.args.value)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.LREM() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.LREM() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_LSET(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key   string
		index string
		value string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.LSET(tt.args.key, tt.args.index, tt.args.value)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.LSET() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.LSET() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_LTRIM(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key   string
		start string
		stop  string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.LTRIM(tt.args.key, tt.args.start, tt.args.stop)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.LTRIM() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.LTRIM() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_RPOP(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.RPOP(tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.RPOP() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.RPOP() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_RPOPLPUSH(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		source      string
		destination string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.RPOPLPUSH(tt.args.source, tt.args.destination)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.RPOPLPUSH() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.RPOPLPUSH() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_RPUSH(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key    string
		field  string
		fields []string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.RPUSH(tt.args.key, tt.args.field, tt.args.fields...)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.RPUSH() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.RPUSH() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_RPUSHX(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key   string
		value string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.RPUSHX(tt.args.key, tt.args.value)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.RPUSHX() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.RPUSHX() = %v, want %v", got, tt.want)
			}
		})
	}
}
