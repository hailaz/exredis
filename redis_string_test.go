package exredis

import (
	"reflect"
	"testing"

	"github.com/gomodule/redigo/redis"
)

func TestRedis_APPEND(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key   string
		value string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int64
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.APPEND(tt.args.key, tt.args.value)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.APPEND() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.APPEND() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_BITCOUNT(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int64
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.BITCOUNT(tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.BITCOUNT() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.BITCOUNT() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_BITCOUNTWithStartEnd(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key   string
		start int64
		end   int64
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int64
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.BITCOUNTWithStartEnd(tt.args.key, tt.args.start, tt.args.end)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.BITCOUNTWithStartEnd() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.BITCOUNTWithStartEnd() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_BITOP(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		args []interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int64
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.BITOP(tt.args.args...)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.BITOP() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.BITOP() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_BITFIELD(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	tests := []struct {
		name   string
		fields fields
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			r.BITFIELD()
		})
	}
}

func TestRedis_DECR(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int64
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.DECR(tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.DECR() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.DECR() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_DECRBY(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key       string
		decrement int64
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int64
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.DECRBY(tt.args.key, tt.args.decrement)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.DECRBY() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.DECRBY() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_GET(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.GET(tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.GET() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.GET() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_GETBIT(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key    string
		offset int64
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.GETBIT(tt.args.key, tt.args.offset)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.GETBIT() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.GETBIT() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_GETRANGE(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key   string
		start int64
		end   int64
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.GETRANGE(tt.args.key, tt.args.start, tt.args.end)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.GETRANGE() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.GETRANGE() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_GETSET(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key   string
		value string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.GETSET(tt.args.key, tt.args.value)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.GETSET() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.GETSET() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_INCR(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int64
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.INCR(tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.INCR() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.INCR() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_INCRBY(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key       string
		decrement int64
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int64
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.INCRBY(tt.args.key, tt.args.decrement)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.INCRBY() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.INCRBY() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_INCRBYFLOAT(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key       string
		decrement float32
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.INCRBYFLOAT(tt.args.key, tt.args.decrement)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.INCRBYFLOAT() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.INCRBYFLOAT() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_MGET(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		args []interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.MGET(tt.args.args...)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.MGET() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Redis.MGET() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_MSET(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		args []interface{}
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			r.MSET(tt.args.args...)
		})
	}
}

func TestRedis_MSETNX(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		args []interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.MSETNX(tt.args.args...)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.MSETNX() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.MSETNX() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_PSETEX(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key          string
		milliseconds int64
		value        string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.PSETEX(tt.args.key, tt.args.milliseconds, tt.args.value)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.PSETEX() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.PSETEX() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_SET(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		args []interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.SET(tt.args.args...)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.SET() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.SET() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_SETBIT(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key    string
		offset int64
		value  int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.SETBIT(tt.args.key, tt.args.offset, tt.args.value)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.SETBIT() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.SETBIT() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_SETEX(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key     string
		seconds int64
		value   string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		// TODO: Add test cases.
		{"case 0", fields{MyRedis.pool}, args{"test12", 1000, "vvvv"}, true, false},
		{"case 0", fields{MyRedis.pool}, args{"test13", 1000, "vvvv"}, true, false},
		{"case 0", fields{MyRedis.pool}, args{"test14", 1000, "vvvv"}, true, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.SETEX(tt.args.key, tt.args.seconds, tt.args.value)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.SETEX() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.SETEX() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_SETNX(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key   string
		value string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		// TODO: Add test cases.
		{"case 0", fields{MyRedis.pool}, args{"test15", "vvvv"}, true, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.SETNX(tt.args.key, tt.args.value)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.SETNX() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.SETNX() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_SETRANGE(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key    string
		offset int64
		value  string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.SETRANGE(tt.args.key, tt.args.offset, tt.args.value)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.SETRANGE() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.SETRANGE() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_STRLEN(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.STRLEN(tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.STRLEN() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.STRLEN() = %v, want %v", got, tt.want)
			}
		})
	}
}
