package exredis

import (
	"reflect"
	"testing"

	"github.com/gomodule/redigo/redis"
)

func TestRedis_ZADD(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key    string
		score  string
		member string
		fields []string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.ZADD(tt.args.key, tt.args.score, tt.args.member, tt.args.fields...)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.ZADD() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.ZADD() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_ZCARD(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.ZCARD(tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.ZCARD() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.ZCARD() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_ZCOUNT(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key string
		min string
		max string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.ZCOUNT(tt.args.key, tt.args.min, tt.args.max)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.ZCOUNT() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.ZCOUNT() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_ZINCRBY(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key       string
		increment int
		member    string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.ZINCRBY(tt.args.key, tt.args.increment, tt.args.member)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.ZINCRBY() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.ZINCRBY() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_ZRANGE(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key   string
		start int
		stop  int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.ZRANGE(tt.args.key, tt.args.start, tt.args.stop)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.ZRANGE() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Redis.ZRANGE() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_ZRANGEWITHSCORES(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key   string
		start int
		stop  int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    map[string]string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.ZRANGEWITHSCORES(tt.args.key, tt.args.start, tt.args.stop)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.ZRANGEWITHSCORES() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Redis.ZRANGEWITHSCORES() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_ZRANGEBYSCORE(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key    string
		min    string
		max    string
		fields []string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.ZRANGEBYSCORE(tt.args.key, tt.args.min, tt.args.max, tt.args.fields...)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.ZRANGEBYSCORE() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Redis.ZRANGEBYSCORE() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_ZRANGEBYSCOREWITHSCORES(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key    string
		min    string
		max    string
		fields []string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    map[string]string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.ZRANGEBYSCOREWITHSCORES(tt.args.key, tt.args.min, tt.args.max, tt.args.fields...)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.ZRANGEBYSCOREWITHSCORES() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Redis.ZRANGEBYSCOREWITHSCORES() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_ZRANK(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key    string
		member string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.ZRANK(tt.args.key, tt.args.member)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.ZRANK() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.ZRANK() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_ZREM(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key     string
		member  string
		members []string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.ZREM(tt.args.key, tt.args.member, tt.args.members...)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.ZREM() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.ZREM() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_ZREMRANGEBYRANK(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key   string
		start int
		stop  int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.ZREMRANGEBYRANK(tt.args.key, tt.args.start, tt.args.stop)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.ZREMRANGEBYRANK() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.ZREMRANGEBYRANK() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_ZREMRANGEBYSCORE(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key string
		min string
		max string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.ZREMRANGEBYSCORE(tt.args.key, tt.args.min, tt.args.max)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.ZREMRANGEBYSCORE() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.ZREMRANGEBYSCORE() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_ZREVRANGE(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key   string
		start int
		stop  int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.ZREVRANGE(tt.args.key, tt.args.start, tt.args.stop)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.ZREVRANGE() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Redis.ZREVRANGE() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_ZREVRANGEWITHSCORES(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key   string
		start int
		stop  int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    map[string]string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.ZREVRANGEWITHSCORES(tt.args.key, tt.args.start, tt.args.stop)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.ZREVRANGEWITHSCORES() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Redis.ZREVRANGEWITHSCORES() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_ZREVRANGEBYSCORE(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key    string
		min    string
		max    string
		fields []string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.ZREVRANGEBYSCORE(tt.args.key, tt.args.min, tt.args.max, tt.args.fields...)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.ZREVRANGEBYSCORE() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Redis.ZREVRANGEBYSCORE() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_ZREVRANGEBYSCOREWITHSCORES(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key    string
		min    string
		max    string
		fields []string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    map[string]string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.ZREVRANGEBYSCOREWITHSCORES(tt.args.key, tt.args.min, tt.args.max, tt.args.fields...)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.ZREVRANGEBYSCOREWITHSCORES() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Redis.ZREVRANGEBYSCOREWITHSCORES() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_ZREVRANK(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key    string
		member string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.ZREVRANK(tt.args.key, tt.args.member)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.ZREVRANK() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.ZREVRANK() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_ZSCORE(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key    string
		member string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.ZSCORE(tt.args.key, tt.args.member)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.ZSCORE() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.ZSCORE() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_ZUNIONSTORE(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	tests := []struct {
		name   string
		fields fields
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			r.ZUNIONSTORE()
		})
	}
}

func TestRedis_ZINTERSTORE(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	tests := []struct {
		name   string
		fields fields
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			r.ZINTERSTORE()
		})
	}
}

func TestRedis_ZSCAN(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	tests := []struct {
		name   string
		fields fields
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			r.ZSCAN()
		})
	}
}

func TestRedis_ZRANGEBYLEX(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key    string
		min    string
		max    string
		fields []string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.ZRANGEBYLEX(tt.args.key, tt.args.min, tt.args.max, tt.args.fields...)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.ZRANGEBYLEX() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Redis.ZRANGEBYLEX() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_ZLEXCOUNT(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key string
		min string
		max string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.ZLEXCOUNT(tt.args.key, tt.args.min, tt.args.max)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.ZLEXCOUNT() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.ZLEXCOUNT() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_ZREMRANGEBYLEX(t *testing.T) {
	type fields struct {
		pool *redis.Pool
	}
	type args struct {
		key string
		min string
		max string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Redis{
				pool: tt.fields.pool,
			}
			got, err := r.ZREMRANGEBYLEX(tt.args.key, tt.args.min, tt.args.max)
			if (err != nil) != tt.wantErr {
				t.Errorf("Redis.ZREMRANGEBYLEX() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Redis.ZREMRANGEBYLEX() = %v, want %v", got, tt.want)
			}
		})
	}
}
